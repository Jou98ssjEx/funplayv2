class Playing extends Phaser.Scene {
    constructor() {
        super('Playing');
    }
    init(data) {
        // Variables Generales

        this.STATE_GAME_NONE = 0;
        this.STATE_GAME_LOADING = 1;
        this.STATE_GAME_PLAYING = 2;
        this.STATE_GAME_GAME_OVER = 3;
        this.STATE_GAME_WIN = 4;
        this.STATE_GAME_PLAYINGIZQ = 5;
        this.STATE_GAME_PLAYINGDERE = 6;
        this.STATE_GAME_PLAYINGP = 7;
        this.stateGame = this.STATE_GAME_NONE;

        this.distanceTrunks = 60;

        this.cursors = this.input.keyboard.createCursorKeys();
        this.pressEnable = true;

        // this.actual_points = 0;
        this.actual_points = 0;
        if (Object.keys(data).length !== 0) {

            this.actual_points = data.points;
        } else {
            this.actual_points = 0;

        }
    }
    preload() {
        console.log('Inicio del juego');
    }
    create() {
        this.bg = this.add.sprite(0, 0, 'background');
        this.bg.setOrigin(0);
        this.tronco = this.add.sprite(480, 210, 'tronco');
        this.man = this.add.sprite(385, 350, 'man_stand');
        this.tomb = this.add.sprite(385, 380, 'tomb');
        this.tomb.visible = false;

        this.dere = this.add.image(800, this.scale.height / 2, 'btnR');
        this.dere.setInteractive({ useHandCursor: true });
        this.dere.setScale(1.5);

        this.izq = this.add.image(120, this.scale.height / 2, 'btnR');
        this.izq.setInteractive({ useHandCursor: true });
        this.izq.setScale(1.5);
        this.izq.flipX = true;

        this.izq.on(Phaser.Input.Events.POINTER_DOWN, () => {

            this.stateGame = this.STATE_GAME_PLAYINGIZQ;
            // this.hitMan(1);
        });
        this.izq.on(Phaser.Input.Events.POINTER_UP, () => {
            this.stateGame = this.STATE_GAME_PLAYINGP;
            this.stateGame = this.STATE_GAME_PLAYING;
        });

        this.dere.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.stateGame = this.STATE_GAME_PLAYINGDERE;
        });
        this.dere.on(Phaser.Input.Events.POINTER_UP, () => {
            this.stateGame = this.STATE_GAME_PLAYINGP;
            this.stateGame = this.STATE_GAME_PLAYING;
        });

        this.points = this.add.bitmapText(
            this.scale.width / 2 - 340,
            80,
            'pixelFont',
            'PUNTOS ' + this.actual_points,
        ).setDepth(2).setOrigin(0.5);

        this.bar = this.add.rectangle(0, 0, this.scale.width, 20, 0x4e9ea5);
        this.bar.setOrigin(0);
        this.trunk = this.add.group();

        for (var i = 0; i < 30; i++) {
            this.trunk1 = this.trunk.create(Phaser.Math.Between(100, 800), Phaser.Math.Between(10, 300), 'trunk');
            this.trunk.killAndHide(this.trunk1);
        }

        this.startGAme();


        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        // Estilos general
        this.style = {
            color: '#000',
            // fontSize: 30,
            font: 'bold 30pt Arial'
        }

        this.style2 = {
            color: '#000',
            // fontSize: 30,
            font: 'bold 16pt Arial'
        }

        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
    }

    refreshBar(value) {
        var newWidth = this.bar.width + value;
        if (newWidth > this.scale.width) {
            newWidth = this.scale.width;
        }
        if (newWidth <= 0) {
            newWidth = 0;
            this.gameOver();
        }
        this.bar.width = newWidth;
    }

    startGAme() {
        this.currentScore = 0;
        this.stateGame = this.STATE_GAME_PLAYING;
        console.log('cki');

        this.sequence = [];
        for (var o = 0; o < 10; o++) {
            this.addTrunk();
        }
        this.printSequence();
    }

    increaseScore() {
        this.actual_points += 100;
        this.points.text = `PUNTOS ${this.actual_points}`;

    }

    addTrunk() {
        this.refreshBar(6);
        this.number = Phaser.Math.Between(-1, 1);
        console.log('nume: ' + this.number);

        if (this.number == 1) {
            //Right
            this.tk = this.trunk.create(this.scale.width / 2 + 90, 280 - (this.sequence.length) * this.distanceTrunks, 'trunk');
            this.tk.direction = 1;
            // console.log('Ejecuto');
            this.tk.flipX = false;
            this.tk.setVisible(true);
            this.sequence.push(this.tk);
        } else
        if (this.number == -1) {
            //Left
            this.tk = this.trunk.create(this.scale.width / 2 - 70, 280 - (this.sequence.length) * this.distanceTrunks, 'trunk');
            this.tk.direction = -1;
            this.tk.flipX = true;
            this.tk.setVisible(true);
            this.sequence.push(this.tk);
        } else {
            //Nothing
            this.sequence.push(null);
            // console.log('Secuencia: ' + this.sequence);
        }

    }

    hitMan(direction) {
        this.sound.play('sfxHit');

        for (var i = 0; i < this.sequence.length; i++) {
            if (this.sequence[i] != null) {
                this.sequence[i].y += this.distanceTrunks;
            }
        }

        var firstTrunk = this.sequence.shift();
        console.log(firstTrunk);
        if (firstTrunk != null) {
            firstTrunk.destroy();
        }

        this.addTrunk();

        //Check GameOver
        var checkTrunk = this.sequence[0];
        if (checkTrunk != null && checkTrunk.direction == direction) {

            // Aqui debe ir el metodo de pregunta
            this.gameOver();

            console.log('perdio');
        } else {
            this.increaseScore();
        }
        this.printSequence();
    }

    // Aqui va el codigo despues de morir

    gameOver() {
        // this.stateGame = this.STATE_GAME_GAME_OVER;

        this.sound.play('sfxGameOver');

        this.man.visible = false;
        this.tomb.visible = true;
        this.tomb.x = this.man.x;

        this.Operaciones();

        // this.scene.start('Menu', { points: this.actual_points });
    }


    PantallaSuma() {
        this.centroW = this.game.config.width / 2;
        this.centroH = this.game.config.height / 2;
        let over = this.add.image(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'tabla').setScale(4);
        this.CW = this.sys.game.config.width / 2;
        this.CH = this.sys.game.config.height / 2;
        let finGAme = this.add.text(this.CW - 190, this.CH - 100, 'Perdiste', this.style);


        // Operacion
        let valor1 = Phaser.Math.Between(0, 9);
        console.log(valor1);
        let valor2 = Phaser.Math.Between(0, 9);
        console.log(valor2);
        let total = valor1 + valor2;


        let tex = this.add.text(this.CW - 175, this.CH - 45, `Para continuar suma: ${valor1} + ${valor2} = ?`, this.style2);

        let r1 = this.physics.add.image(this.CW - 125, this.CH + 23, 'buno');
        let n1 = this.add.text(this.CW - 122, this.CH + 13, '6', this.style2);

        let r2 = this.physics.add.image(this.CW - 20, this.CH + 23, 'bdos');
        let n2 = this.add.text(this.CW - 17, this.CH + 13, '7', this.style2);

        let r3 = this.physics.add.image(this.CW + 85, this.CH + 23, 'btres');
        let n3 = this.add.text(this.CW + 88, this.CH + 13, '8', this.style2);

        this.final = this.add.text(this.CW + 83, this.CH + 60, `${total}.`, this.style2);
        this.final.visible = false;

        let tex2 = this.add.text(this.CW - 175, this.CH + 60, `La respuesta corecta es:  `, this.style2);

        n1.setInteractive({ useHandCursor: true });
        n2.setInteractive({ useHandCursor: true });
        n3.setInteractive({ useHandCursor: true });

        n1.text = Phaser.Math.Between(0, 15);
        n2.text = Phaser.Math.Between(0, 15);
        n3.text = Phaser.Math.Between(0, 15);


        this.valores = [];

        this.valores.push(n1.text);
        this.valores.push(n2.text);
        this.valores.push(n3.text);

        console.log(this.valores);


        let int = Phaser.Math.Between(0, 2);

        this.b = this.valores[int]
        console.log(this.b);
        this.b = total
        console.log(this.b);

        if (int == 0) {
            n1.text = this.b;
        }
        if (int == 1) {
            n2.text = this.b;
        }
        if (int == 2) {
            n3.text = this.b;
        }


        let contenedorF = this.add.container(0, -400);
        contenedorF.add([over, finGAme, tex, r1, n1, r2, n2, r3, n3, tex2, this.final]);

        this.tweens.add({
            targets: contenedorF,
            ease: 'Pointer1',
            y: 0
        });

        n1.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n1.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });
                    }
                })
            }
        });
        n2.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n2.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });

                    }
                })

            }
        });
        n3.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n3.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });
                    }
                })

            }
        });

    }
    PantallaMultiplica() {
        this.centroW = this.game.config.width / 2;
        this.centroH = this.game.config.height / 2;
        let over = this.add.image(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'tabla').setScale(4);
        this.CW = this.sys.game.config.width / 2;
        this.CH = this.sys.game.config.height / 2;
        let finGAme = this.add.text(this.CW - 190, this.CH - 100, 'Perdiste', this.style);


        // Operacion
        let valor1 = Phaser.Math.Between(0, 9);
        console.log(valor1);
        let valor2 = Phaser.Math.Between(0, 9);
        console.log(valor2);
        let total = valor1 * valor2;


        let tex = this.add.text(this.CW - 175, this.CH - 45, `Para continuar multiplica: ${valor1} x ${valor2} = ?`, this.style2);

        let r1 = this.physics.add.image(this.CW - 125, this.CH + 23, 'buno');
        let n1 = this.add.text(this.CW - 122, this.CH + 13, '6', this.style2);

        let r2 = this.physics.add.image(this.CW - 20, this.CH + 23, 'bdos');
        let n2 = this.add.text(this.CW - 17, this.CH + 13, '7', this.style2);

        let r3 = this.physics.add.image(this.CW + 85, this.CH + 23, 'btres');
        let n3 = this.add.text(this.CW + 88, this.CH + 13, '8', this.style2);

        this.final = this.add.text(this.CW + 83, this.CH + 60, `${total}.`, this.style2);
        this.final.visible = false;

        let tex2 = this.add.text(this.CW - 175, this.CH + 60, `La respuesta corecta es:  `, this.style2);

        n1.setInteractive({ useHandCursor: true });
        n2.setInteractive({ useHandCursor: true });
        n3.setInteractive({ useHandCursor: true });

        n1.text = Phaser.Math.Between(0, 15);
        n2.text = Phaser.Math.Between(0, 45);
        n3.text = Phaser.Math.Between(0, 75);


        this.valores = [];

        this.valores.push(n1.text);
        this.valores.push(n2.text);
        this.valores.push(n3.text);

        console.log(this.valores);


        let int = Phaser.Math.Between(0, 2);

        this.b = this.valores[int]
        console.log(this.b);
        this.b = total
        console.log(this.b);

        if (int == 0) {
            n1.text = this.b;
        }
        if (int == 1) {
            n2.text = this.b;
        }
        if (int == 2) {
            n3.text = this.b;
        }


        let contenedorF = this.add.container(0, -400);
        contenedorF.add([over, finGAme, tex, r1, n1, r2, n2, r3, n3, tex2, this.final]);

        this.tweens.add({
            targets: contenedorF,
            ease: 'Pointer1',
            y: 0
        });

        n1.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n1.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });
                    }
                })
            }
        });
        n2.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n2.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });

                    }
                })

            }
        });
        n3.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n3.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });
                    }
                })

            }
        });

    }
    PantallaResta() {
        this.centroW = this.game.config.width / 2;
        this.centroH = this.game.config.height / 2;
        let over = this.add.image(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'tabla').setScale(4);
        this.CW = this.sys.game.config.width / 2;
        this.CH = this.sys.game.config.height / 2;
        let finGAme = this.add.text(this.CW - 190, this.CH - 100, 'Perdiste', this.style);


        // Operacion
        let valor1 = Phaser.Math.Between(5, 10);
        console.log(valor1);
        let valor2 = Phaser.Math.Between(0, 5);
        console.log(valor2);

        // if (valor2 > valor1) {
        //     valor1 = Phaser.Math.Between(0, 9);
        // }
        let total = valor1 - valor2;


        let tex = this.add.text(this.CW - 175, this.CH - 45, `Para continuar resta: ${valor1} - ${valor2} = ?`, this.style2);

        let r1 = this.physics.add.image(this.CW - 125, this.CH + 23, 'buno');
        let n1 = this.add.text(this.CW - 122, this.CH + 13, '6', this.style2);

        let r2 = this.physics.add.image(this.CW - 20, this.CH + 23, 'bdos');
        let n2 = this.add.text(this.CW - 17, this.CH + 13, '7', this.style2);

        let r3 = this.physics.add.image(this.CW + 85, this.CH + 23, 'btres');
        let n3 = this.add.text(this.CW + 88, this.CH + 13, '8', this.style2);

        this.final = this.add.text(this.CW + 83, this.CH + 60, `${total}.`, this.style2);
        this.final.visible = false;

        let tex2 = this.add.text(this.CW - 175, this.CH + 60, `La respuesta corecta es:  `, this.style2);

        n1.setInteractive({ useHandCursor: true });
        n2.setInteractive({ useHandCursor: true });
        n3.setInteractive({ useHandCursor: true });

        n1.text = Phaser.Math.Between(0, 15);
        n2.text = Phaser.Math.Between(0, 15);
        n3.text = Phaser.Math.Between(0, 15);


        this.valores = [];

        this.valores.push(n1.text);
        this.valores.push(n2.text);
        this.valores.push(n3.text);

        console.log(this.valores);


        let int = Phaser.Math.Between(0, 2);

        this.b = this.valores[int]
        console.log(this.b);
        this.b = total
        console.log(this.b);

        if (int == 0) {
            n1.text = this.b;
        }
        if (int == 1) {
            n2.text = this.b;
        }
        if (int == 2) {
            n3.text = this.b;
        }


        let contenedorF = this.add.container(0, -400);
        contenedorF.add([over, finGAme, tex, r1, n1, r2, n2, r3, n3, tex2, this.final]);

        this.tweens.add({
            targets: contenedorF,
            ease: 'Pointer1',
            y: 0
        });

        n1.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n1.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });
                    }
                })
            }
        });
        n2.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n2.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });

                    }
                })

            }
        });
        n3.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n3.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });
                    }
                })

            }
        });
    }
    PantallaDivide() {
        this.centroW = this.game.config.width / 2;
        this.centroH = this.game.config.height / 2;
        let over = this.add.image(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'tabla').setScale(4);
        this.CW = this.sys.game.config.width / 2;
        this.CH = this.sys.game.config.height / 2;
        let finGAme = this.add.text(this.CW - 190, this.CH - 100, 'Perdiste', this.style);

        let nueve = [9, 3, 1];
        let ocho = [8, 4, 2, 1];
        let siete = [7, 1];
        let seis = [6, 3, 1];
        let cinco = [5, 1];
        let cuatro = [4, 2, 1];
        let tres = [3, 1];
        let dos = [2, 1];
        let uno = [1];
        let cero = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];


        // Operacion
        let valor1 = Phaser.Math.Between(0, 9);
        console.log(valor1);

        if (valor1 == 9) {
            let ent = nueve[Phaser.Math.Between(0, 2)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 8) {
            let ent = ocho[Phaser.Math.Between(0, 3)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 7) {
            let ent = siete[Phaser.Math.Between(0, 1)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 6) {
            let ent = seis[Phaser.Math.Between(0, 2)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 5) {
            let ent = cinco[Phaser.Math.Between(0, 1)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 4) {
            let ent = cuatro[Phaser.Math.Between(0, 2)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 3) {
            let ent = tres[Phaser.Math.Between(0, 1)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 2) {
            let ent = dos[Phaser.Math.Between(0, 1)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 1) {
            let ent = uno[0];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 0) {
            let ent = cero[Phaser.Math.Between(0, 9)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }



        // if (valor2 > valor1) {
        //     valor1 = Phaser.Math.Between(0, 9);
        //     let total = valor1 / valor2;

        // if (total)

        // if (valor2 == 3 || valor2 == 5 || valor2 == 7 || valor2 == 9){

        // }


        let tex = this.add.text(this.CW - 175, this.CH - 45, `Para continuar divide: ${this.N1} / ${this.N2} = ?`, this.style2);

        let r1 = this.physics.add.image(this.CW - 125, this.CH + 23, 'buno');
        let n1 = this.add.text(this.CW - 122, this.CH + 13, '6', this.style2);

        let r2 = this.physics.add.image(this.CW - 20, this.CH + 23, 'bdos');
        let n2 = this.add.text(this.CW - 17, this.CH + 13, '7', this.style2);

        let r3 = this.physics.add.image(this.CW + 85, this.CH + 23, 'btres');
        let n3 = this.add.text(this.CW + 88, this.CH + 13, '8', this.style2);

        this.final = this.add.text(this.CW + 83, this.CH + 60, `${this.R}.`, this.style2);
        this.final.visible = false;

        let tex2 = this.add.text(this.CW - 175, this.CH + 60, `La respuesta corecta es:  `, this.style2);

        n1.setInteractive({ useHandCursor: true });
        n2.setInteractive({ useHandCursor: true });
        n3.setInteractive({ useHandCursor: true });

        n1.text = Phaser.Math.Between(0, 15);
        n2.text = Phaser.Math.Between(0, 15);
        n3.text = Phaser.Math.Between(0, 15);


        this.valores = [];

        this.valores.push(n1.text);
        this.valores.push(n2.text);
        this.valores.push(n3.text);

        console.log(this.valores);


        let int = Phaser.Math.Between(0, 2);

        this.b = this.valores[int]
        console.log(this.b);
        this.b = this.R
        console.log(this.b);

        if (int == 0) {
            n1.text = this.b;
        }
        if (int == 1) {
            n2.text = this.b;
        }
        if (int == 2) {
            n3.text = this.b;
        }


        let contenedorF = this.add.container(0, -400);
        contenedorF.add([over, finGAme, tex, r1, n1, r2, n2, r3, n3, tex2, this.final]);

        this.tweens.add({
            targets: contenedorF,
            ease: 'Pointer1',
            y: 0
        });

        n1.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n1.text == this.R) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });
                    }
                })
            }
        });
        n2.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n2.text == this.R) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });

                    }
                })

            }
        });
        n3.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n3.text == this.R) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Playing', { points: this.actual_points });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.actual_points });
                    }
                })

            }
        });
        // }

    }

    Operaciones() {

        // let OP = [this.PantallaSuma(), this.PantallaResta(), this.PantallaMultiplica(), this.PantallaDivide()];
        let C = Phaser.Math.Between(0, 3);
        if (C == 0) {
            this.PantallaSuma();
        }
        if (C == 1) {
            this.PantallaResta();
        }
        if (C == 2) {
            this.PantallaMultiplica();
        }
        if (C == 3) {
            this.PantallaDivide();
        }
        // return C;
        // }
    }

    printSequence() {
        var stringSequence = "";
        for (var i = 0; i < this.sequence.length; i++) {
            if (this.sequence[i] == null) {
                stringSequence += "0,";
            } else {
                stringSequence += this.sequence[i].direction + ",";
            }
        }
        console.log(stringSequence);
    }

    update(time, delta) {

        switch (this.stateGame) {
            case this.STATE_GAME_NONE:

                break;
            case this.STATE_GAME_LOADING:

                break;
            case this.STATE_GAME_PLAYING:

                this.refreshBar(-0.5);

                if (this.cursors.left.isDown && this.pressEnable) {
                    this.pressEnable = false;
                    console.log('left');
                    this.man.x = 385;
                    this.man.flipX = false;
                    this.man.setTexture('man_hit');
                    this.hitMan(-1);
                }

                if (this.cursors.right.isDown && this.pressEnable) {
                    this.pressEnable = false;
                    console.log('right');
                    this.man.x = 570;
                    this.man.flipX = true;
                    this.man.setTexture('man_hit');
                    this.hitMan(1);
                }

                if (this.cursors.left.isUp && this.cursors.right.isUp) {
                    this.pressEnable = true;
                    this.man.setTexture('man_stand');
                }
                break;


            case this.STATE_GAME_GAME_OVER:
                console.log("GAME OVER");
                break;
            case this.STATE_GAME_WIN:

                break;

            case this.STATE_GAME_PLAYINGIZQ:
                this.refreshBar(-0.5);
                if (this.pressEnable) {
                    this.pressEnable = false;
                    console.log('left');
                    this.man.x = 385;
                    this.man.flipX = false;
                    this.man.setTexture('man_hit');
                    this.hitMan(-1);
                }
                break;

            case this.STATE_GAME_PLAYINGDERE:
                this.refreshBar(-0.5);
                if (this.pressEnable) {
                    this.pressEnable = false;
                    console.log('right');
                    this.man.x = 570;
                    this.man.flipX = true;
                    this.man.setTexture('man_hit');
                    this.hitMan(1);
                }
                break;

            case this.STATE_GAME_PLAYINGP:
                this.pressEnable = true;
                this.man.setTexture('man_stand');
                break;


        }
    }
}
export default Playing;