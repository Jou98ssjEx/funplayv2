import BootLoader from '/src/lenador/Bootloader.js';
import Menu from '/src/lenador/scenes/Menu.js';
import Playing from '/src/lenador/scenes/Playing.js';



const lienzo = document.getElementById('ingreso');
// 1295
// 792

const config = {
    width: 940,
    height: 440,
    parent: "ingreso",
    // background: ruta,
    type: Phaser.AUTO,
    pixelArt: true,
    scene: [
        BootLoader,
        Menu,
        Playing,
        // UI,

    ],
    physics: {
        default: "arcade",
        arcade: {
            gravity: {
                // y: 500
                y: 2000
            },
            // debug: true
        }
    },
    scale: {
        mode: Phaser.ScaleModes.SHOW_ALL
            // mode: Phaser.Scale.FIT,
            // autoCenter: Phaser.Scale.CENTER_BOTH
    },
    dom: {
        // createContainer: truek
    },
}

const game = new Phaser.Game(config);
export default game;