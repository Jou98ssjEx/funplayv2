class Menu extends Phaser.Scene {
    constructor() {
        super('Menu');

    }

    preload() {
        console.log('Menu Principal');
    }



    create() {

        this.back = this.add.image(0, 0, "bg");
        this.back.setOrigin(0, 0);

        this.btnini = this.add.image(460, -330, 'btnIni').setInteractive({ useHandCursor: true });
        this.lblSalir = this.add.image(70, -400, 'lbSalir').setInteractive({ useHandCursor: true });
        this.lblExpan = this.add.image(850, -400, 'lbExpan').setInteractive({ useHandCursor: true });

        this.btnini.on('pointerdown', function(pointer) {
            this.setTint(0x4f5bb8);
        });

        // Cuando el usuario abandona el boton
        this.btnini.on('pointerout', function(pointer) {
            this.clearTint();
        });

        // Con esta funcion cambias la escena a traves del evento del boton ---Actualiza
        this.btnini.on('pointerup', () => {
            this.scene.start('Play', 'fin');
        });

        // Exit btn

        this.lblSalir.on('pointerdown', function(pointer) {
            this.setTint(0xff3243);
        });
        this.lblSalir.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.lblSalir.on('pointerup', function(pointer) {
            location.href = '../start'
                // this.clearTint();
        });



        // fullscreen
        const fullscreen = (e) => {
                if (e.webkitRequestFullScreen) {
                    e.webkitRequestFullScreen();
                } else if (e.mozRequestFullScreen) {
                    e.mozRequestFullScreen();
                }
            }
            // Botn para el fullScreen
        this.lblExpan.on('pointerdown', function(pointer) {
            this.setTint(0x4e9ea5);
        });
        this.lblExpan.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.lblExpan.on('pointerup', () => {
            fullscreen(document.getElementById('ingreso'));
        });

        // // Titulo
        // this.non = this.physics.add.image(-650, -100, "non");
        // this.non.setScale(1.3);

        // Animacion del titulo
        let tween_titulo = this.tweens.add({
            targets: [this.btnini],
            props: {
                x: {
                    value: 460,
                    duration: 1000
                },
                y: {
                    value: 330,
                    duration: 1000
                }
            },
            delay: 1000
        });
        // Aminacion del personaje
        let tween_caballo = this.tweens.add({
            targets: [this.lblSalir],
            props: {
                x: {
                    value: 70,
                    duration: 1000
                },
                y: {
                    value: 400,
                    duration: 1000
                }
            },
            delay: 1000
        });
        // Animación de los botones del menu
        let tween_btn_play = this.tweens.add({
            targets: [this.lblExpan],
            props: {
                x: {
                    value: 850,
                    duration: 1000
                },
                y: {
                    value: 400,
                    duration: 1000
                }
            },
            delay: 1000
        });
        // // let tween_btn_info = this.tweens.add({
        // //     targets: [this.btn_info],
        // //     props: {
        // //         x: {
        // //             value: 300,
        // //             duration: 1000
        // //         },
        // //         y: {
        // //             value: 200,
        // //             duration: 1000
        // //         }
        // //     },
        // //     delay: 1000
        // // });
        // let tween_btn_exit = this.tweens.add({
        //     targets: [this.btn_exit],
        //     props: {
        //         x: {
        //             value: 300,
        //             duration: 1000
        //         },
        //         y: {
        //             value: 220,
        //             duration: 1000
        //         }
        //     },
        //     delay: 1000
        // });

        // // Animación de botones secundarios
        // // let tween_btn = this.tweens.add({
        // //     targets: [this.btn],
        // //     props: {
        // //         x: {
        // //             value: 40,
        // //             duration: 1000
        // //         },
        // //         y: {
        // //             value: 410,
        // //             duration: 1000
        // //         }
        // //     },
        // //     delay: 1000
        // // });
        // // let tween_btn1 = this.tweens.add({
        // //     targets: [this.btn1],
        // //     props: {
        // //         x: {
        // //             value: 100,
        // //             duration: 1000
        // //         },
        // //         y: {
        // //             value: 410,
        // //             duration: 1000
        // //         }
        // //     },
        // //     delay: 1000
        // // });
        // // let tween_btn2 = this.tweens.add({
        // //     targets: [this.btn2],
        // //     props: {
        // //         x: {
        // //             value: 160,
        // //             duration: 1000
        // //         },
        // //         y: {
        // //             value: 410,
        // //             duration: 1000
        // //         }
        // //     },
        // //     delay: 1000
        // // });
        // let tween_btn3 = this.tweens.add({
        //     targets: [this.btn3],
        //     props: {
        //         x: {
        //             value: 880,
        //             duration: 1000
        //         },
        //         y: {
        //             value: 410,
        //             duration: 1000
        //         }
        //     },
        //     delay: 1000
        // });


        // // Selecion dentro y fuera del canvas
        // var graphics = this.add.graphics();

        // graphics.fillStyle(0x000000, 0.5);
        // graphics.fillRect(0, 0, 940, 440);
        // graphics.setVisible(false);

        // this.input.on('gameout', function() {
        //     graphics.setVisible(true);
        // });
        // this.input.on('gameover', function() {
        //     graphics.setVisible(false);
        // });

    }




    update(time, delta) {

    }

}
export default Menu;