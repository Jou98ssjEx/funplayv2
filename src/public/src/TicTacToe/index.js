import BootLoader from '/src/TicTacToe/Bootloader.js';
import Reload from '/src/TicTacToe/scenes/Reload.js';
import Menu from '/src/TicTacToe/scenes/Menu.js';
import Play from '/src/TicTacToe/scenes/Play.js';

// import Playing from '/src/scenes/Playing.js';


const lienzo = document.getElementById('ingreso');
// 1295
// 792

const config = {
    width: 940,
    height: 440,
    parent: "ingreso",
    // background: ruta,
    type: Phaser.AUTO,
    pixelArt: true,
    scene: [
        BootLoader,
        Menu,
        Play,
        Reload,

    ],
    scale: {
        mode: Phaser.ScaleModes.SHOW_ALL
    }
}

const game = new Phaser.Game(config);
export default game;