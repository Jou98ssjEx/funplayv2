class UI extends Phaser.Scene {
    constructor() {
        super({ key: 'UI' });
    }

    init(data) {
        console.log('Se ha iniciado la escena UI');
        this.scene.moveUp();
        this.actual_points = 0;
        // console.log(data.points);
        // this.actual_points = data.points1;
        if (Object.keys(data).length !== 0) {
            // this.points.setText(Phaser.Utils.String.Pad(`${data.points1}`, 6, '0', 1));
            // this.points = data.points1;
            // this.poi = data.points;
            this.actual_points = data.points;
            // console.log(this.actual_points);
        } else {
            this.actual_points = 0;

        }
    }

    create() {
        this.groupLife = this.add.group({
            key: 'life',
            repeat: 2,
            setXY: {
                x: 50,
                y: 20,
                stepX: 25
            }
        });

        this.points = this.add.bitmapText(
            this.scale.width - 40,
            20,
            'pixelFont',
            Phaser.Utils.String.Pad(`${this.actual_points}`, 6, '0', 1)
        ).setOrigin(1, 0).setTint(0x000000);

        // this.points = this.add.bitmapText(
        //     this.scale.width - 40,
        //     20,
        //     'pixelFont',
        //     'PUNTOS ' + this.actual_points,
        //     Phaser.Utils.String.Pad('0', 6, '0', 1)
        // ).setOrigin(1, 0).setTint(0x000000);




        // Eventos
        this.registry.events.on('remove_life', () => {
            this.groupLife.getChildren()[this.groupLife.getChildren().length - 1].destroy();
            // alert('Aqui');
            // console.log(this.groupLife.getChildren().length);
            // if (this.groupLife.getChildren().length == 0) {
            //     this.PantallaSuma();
            // }
        });
        this.registry.events.on('game_over', () => {
            // this.PantallaSuma();
            this.registry.events.removeAllListeners();
            // console.log(this.registry.events.removeAllListeners());
            // this.scene.remove('Play');
            this.scene.start('OP', { points: this.actual_points });
        });

        this.registry.events.on('update_points', () => {
            this.actual_points += 10;
            this.points.setText(Phaser.Utils.String.Pad(this.actual_points, 6, '0', 1));
            // this.points.setText('PUNTOS ' + this.actual_points);
        });

        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        // Estilos general
        this.style = {
            color: '#000',
            // fontSize: 30,
            font: 'bold 30pt Arial'
        }

        this.style2 = {
            color: '#000',
            // fontSize: 30,
            font: 'bold 16pt Arial'
        }

        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
    }

}

export default UI;