class OP extends Phaser.Scene {
    constructor() {
        super({ key: 'OP' });
    }
    init(data) {
        console.log('Se ha iniciado la escena Operaciones');
        // this.scene.launch('UI');
        // this.points = 0;

        if (Object.keys(data).length !== 0) {
            this.points1 = data.points;
        }
        // this.STATE_GAME_PLAYI, { points: this.actual_points }NG = 1;

        // this.STATE_GAME_PLAYI, { points: this.actual_points }NGIZQ = 5;
        // this.STATE_GAME_PLAYINGDERE = 6;
        // this.STATE_GAME_PLAYINGP = 7;

        // this.stateGame = this.STATE_GAME_PLAYING;

        // console.log();
    }

    create() {
        this.add.image(0, 0, 'background')
            .setOrigin(0);

        // this.dere = this.add.image(800, this.scale.height / 2, 'btnR');
        // this.dere.setTint(0xfff);
        // this.dere.setInteractive({ useHandCursor: true });
        // this.dere.setScale(1.5);

        // this.izq = this.add.image(120, this.scale.height / 2, 'btnR');
        // this.izq.setTint(0xfff);
        // this.izq.setInteractive({ useHandCursor: true });
        // this.izq.setScale(1.5);
        // this.izq.flipX = true;

        // this.arriba = this.add.image(70, this.scale.height / 2, 'btnR');
        // this.arriba.setInteractive({ useHandCursor: true });
        // this.arriba.flipX = true;
        // this.arriba.setTint(0xfff);
        // this.arriba.setScale(1.5);
        // this.arriba.setRotation(1.5708);

        // this.arriba1 = this.add.image(860, this.scale.height / 2, 'btnR');
        // this.arriba1.setInteractive({ useHandCursor: true });
        // this.arriba1.flipX = true;
        // this.arriba1.setTint(0xfff);
        // this.arriba1.setScale(1.5);
        // this.arriba1.setRotation(1.5708);



        this.wall_floor = this.physics.add.staticGroup();

        this.wall_floor.create(0, 0, 'wall')
            .setOrigin(0);
        this.wall_floor.create(this.scale.width, 0, 'wall')
            .setOrigin(1, 0)
            .setFlipX(true);

        this.wall_floor.create(-13, this.scale.height, 'floor')
            .setOrigin(0, 1);

        this.wall_floor.refresh();

        this.wall_floor.getChildren()[2].setOffset(0, 15);

        this.btnCAM = this.add.image(880, 420, 'btnCAM').setInteractive({ useHandCursor: true });
        // fullscreen
        const fullscreen = (e) => {
                if (e.webkitRequestFullScreen) {
                    e.webkitRequestFullScreen();
                } else if (e.mozRequestFullScreen) {
                    e.mozRequestFullScreen();
                }
            }
            // Botn para el fullScreen
        this.btnCAM.on('pointerdown', function(pointer) {
            this.setTint(0x4e9ea5);
        });
        this.btnCAM.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btnCAM.on('pointerup', () => {
            fullscreen(document.getElementById('ingreso'));
        });


        // // Bombs
        // this.bombsGroup = new Bombs({
        //     physicsWorld: this.physics.world,
        //     scene: this
        // });

        // // Items
        // this.itemsGroup = new TomatoItem({
        //     physicsWorld: this.physics.world,
        //     scene: this
        // });

        // // Personaje
        // this.tomato = new Tomato({
        //     scene: this,
        //     x: 100,
        //     y: 100,
        // });


        this.pointsText = this.add.bitmapText(
            this.scale.width / 2,
            20,
            'pixelFont',
            'PUNTOS ' + this.points1
        ).setDepth(2).setOrigin(0.5);
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        // Estilos general
        this.style = {
            color: '#000',
            // fontSize: 30,
            font: 'bold 30pt Arial'
        }

        this.style2 = {
            color: '#000',
            // fontSize: 30,
            font: 'bold 16pt Arial'
        }

        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////



        // let aRR = [];

        // this.physics.add.collider([this.tomato, this.bombsGroup], this.wall_floor);
        // this.physics.add.overlap(this.tomato, this.bombsGroup, () => {
        //     this.tomato.bombCollision();
        //     console.log(this.tomato.bombCollision());

        // });

        // this.physics.add.overlap(this.itemsGroup, this.tomato, () => {
        //     this.sound.play('pop');
        //     this.registry.events.emit('update_points');
        //     this.itemsGroup.destroyItem();
        //     this.Bo = this.bombsGroup.addBomb();
        //     // this.cont = 1;
        //     aRR.push(this.Bo);
        //     console.log(this.Bo);
        // });

        // this.Nume = this.cont ;
        // console.log(this.Nume);
        // this.btnR = this.input.keyboard.createCursorKeys();
        // this.btnR.on(Phaser.Input.Events.POINTER_DOWN, () => {
        //     // alert('HOLA');

        // });

        // 
        this.Operaciones();
    }

    PantallaSuma() {
        this.centroW = this.game.config.width / 2;
        this.centroH = this.game.config.height / 2;
        let over = this.add.image(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'tabla').setScale(4);
        this.CW = this.sys.game.config.width / 2;
        this.CH = this.sys.game.config.height / 2;
        let finGAme = this.add.text(this.CW - 190, this.CH - 100, 'Perdiste', this.style);


        // Operacion
        let valor1 = Phaser.Math.Between(0, 9);
        console.log(valor1);
        let valor2 = Phaser.Math.Between(0, 9);
        console.log(valor2);
        let total = valor1 + valor2;


        let tex = this.add.text(this.CW - 175, this.CH - 45, `Para continuar suma: ${valor1} + ${valor2} = ?`, this.style2);

        let r1 = this.physics.add.image(this.CW - 125, this.CH + 23, 'buno');
        let n1 = this.add.text(this.CW - 122, this.CH + 13, '6', this.style2);

        let r2 = this.physics.add.image(this.CW - 20, this.CH + 23, 'bdos');
        let n2 = this.add.text(this.CW - 17, this.CH + 13, '7', this.style2);

        let r3 = this.physics.add.image(this.CW + 85, this.CH + 23, 'btres');
        let n3 = this.add.text(this.CW + 88, this.CH + 13, '8', this.style2);

        this.final = this.add.text(this.CW + 83, this.CH + 60, `${total}.`, this.style2);
        this.final.visible = false;

        let tex2 = this.add.text(this.CW - 175, this.CH + 60, `La respuesta corecta es:  `, this.style2);

        n1.setInteractive({ useHandCursor: true });
        n2.setInteractive({ useHandCursor: true });
        n3.setInteractive({ useHandCursor: true });

        n1.text = Phaser.Math.Between(0, 15);
        n2.text = Phaser.Math.Between(0, 15);
        n3.text = Phaser.Math.Between(0, 15);


        this.valores = [];

        this.valores.push(n1.text);
        this.valores.push(n2.text);
        this.valores.push(n3.text);

        console.log(this.valores);


        let int = Phaser.Math.Between(0, 2);

        this.b = this.valores[int]
        console.log(this.b);
        this.b = total
        console.log(this.b);

        if (int == 0) {
            n1.text = this.b;
        }
        if (int == 1) {
            n2.text = this.b;
        }
        if (int == 2) {
            n3.text = this.b;
        }


        let contenedorF = this.add.container(0, -400);
        contenedorF.add([over, finGAme, tex, r1, n1, r2, n2, r3, n3, tex2, this.final]);

        this.tweens.add({
            targets: contenedorF,
            ease: 'Pointer1',
            y: 0
        });

        n1.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n1.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });
                    }
                })
            }
        });
        n2.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n2.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });

                    }
                })

            }
        });
        n3.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n3.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });
                    }
                })

            }
        });

    }
    PantallaMultiplica() {
        this.centroW = this.game.config.width / 2;
        this.centroH = this.game.config.height / 2;
        let over = this.add.image(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'tabla').setScale(4);
        this.CW = this.sys.game.config.width / 2;
        this.CH = this.sys.game.config.height / 2;
        let finGAme = this.add.text(this.CW - 190, this.CH - 100, 'Perdiste', this.style);


        // Operacion
        let valor1 = Phaser.Math.Between(0, 9);
        console.log(valor1);
        let valor2 = Phaser.Math.Between(0, 9);
        console.log(valor2);
        let total = valor1 * valor2;


        let tex = this.add.text(this.CW - 175, this.CH - 45, `Para continuar multiplica: ${valor1} x ${valor2} = ?`, this.style2);

        let r1 = this.physics.add.image(this.CW - 125, this.CH + 23, 'buno');
        let n1 = this.add.text(this.CW - 122, this.CH + 13, '6', this.style2);

        let r2 = this.physics.add.image(this.CW - 20, this.CH + 23, 'bdos');
        let n2 = this.add.text(this.CW - 17, this.CH + 13, '7', this.style2);

        let r3 = this.physics.add.image(this.CW + 85, this.CH + 23, 'btres');
        let n3 = this.add.text(this.CW + 88, this.CH + 13, '8', this.style2);

        this.final = this.add.text(this.CW + 83, this.CH + 60, `${total}.`, this.style2);
        this.final.visible = false;

        let tex2 = this.add.text(this.CW - 175, this.CH + 60, `La respuesta corecta es:  `, this.style2);

        n1.setInteractive({ useHandCursor: true });
        n2.setInteractive({ useHandCursor: true });
        n3.setInteractive({ useHandCursor: true });

        n1.text = Phaser.Math.Between(0, 15);
        n2.text = Phaser.Math.Between(0, 45);
        n3.text = Phaser.Math.Between(0, 75);


        this.valores = [];

        this.valores.push(n1.text);
        this.valores.push(n2.text);
        this.valores.push(n3.text);

        console.log(this.valores);


        let int = Phaser.Math.Between(0, 2);

        this.b = this.valores[int]
        console.log(this.b);
        this.b = total
        console.log(this.b);

        if (int == 0) {
            n1.text = this.b;
        }
        if (int == 1) {
            n2.text = this.b;
        }
        if (int == 2) {
            n3.text = this.b;
        }


        let contenedorF = this.add.container(0, -400);
        contenedorF.add([over, finGAme, tex, r1, n1, r2, n2, r3, n3, tex2, this.final]);

        this.tweens.add({
            targets: contenedorF,
            ease: 'Pointer1',
            y: 0
        });

        n1.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n1.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });
                    }
                })
            }
        });
        n2.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n2.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });

                    }
                })

            }
        });
        n3.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n3.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });
                    }
                })

            }
        });

    }
    PantallaResta() {
        this.centroW = this.game.config.width / 2;
        this.centroH = this.game.config.height / 2;
        let over = this.add.image(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'tabla').setScale(4);
        this.CW = this.sys.game.config.width / 2;
        this.CH = this.sys.game.config.height / 2;
        let finGAme = this.add.text(this.CW - 190, this.CH - 100, 'Perdiste', this.style);


        // Operacion
        let valor1 = Phaser.Math.Between(5, 10);
        console.log(valor1);
        let valor2 = Phaser.Math.Between(0, 5);
        console.log(valor2);

        // if (valor2 > valor1) {
        //     valor1 = Phaser.Math.Between(0, 9);
        // }
        let total = valor1 - valor2;


        let tex = this.add.text(this.CW - 175, this.CH - 45, `Para continuar resta: ${valor1} - ${valor2} = ?`, this.style2);

        let r1 = this.physics.add.image(this.CW - 125, this.CH + 23, 'buno');
        let n1 = this.add.text(this.CW - 122, this.CH + 13, '6', this.style2);

        let r2 = this.physics.add.image(this.CW - 20, this.CH + 23, 'bdos');
        let n2 = this.add.text(this.CW - 17, this.CH + 13, '7', this.style2);

        let r3 = this.physics.add.image(this.CW + 85, this.CH + 23, 'btres');
        let n3 = this.add.text(this.CW + 88, this.CH + 13, '8', this.style2);

        this.final = this.add.text(this.CW + 83, this.CH + 60, `${total}.`, this.style2);
        this.final.visible = false;

        let tex2 = this.add.text(this.CW - 175, this.CH + 60, `La respuesta corecta es:  `, this.style2);

        n1.setInteractive({ useHandCursor: true });
        n2.setInteractive({ useHandCursor: true });
        n3.setInteractive({ useHandCursor: true });

        n1.text = Phaser.Math.Between(0, 15);
        n2.text = Phaser.Math.Between(0, 15);
        n3.text = Phaser.Math.Between(0, 15);


        this.valores = [];

        this.valores.push(n1.text);
        this.valores.push(n2.text);
        this.valores.push(n3.text);

        console.log(this.valores);


        let int = Phaser.Math.Between(0, 2);

        this.b = this.valores[int]
        console.log(this.b);
        this.b = total
        console.log(this.b);

        if (int == 0) {
            n1.text = this.b;
        }
        if (int == 1) {
            n2.text = this.b;
        }
        if (int == 2) {
            n3.text = this.b;
        }


        let contenedorF = this.add.container(0, -400);
        contenedorF.add([over, finGAme, tex, r1, n1, r2, n2, r3, n3, tex2, this.final]);

        this.tweens.add({
            targets: contenedorF,
            ease: 'Pointer1',
            y: 0
        });

        n1.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n1.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });
                    }
                })
            }
        });
        n2.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n2.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });

                    }
                })

            }
        });
        n3.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n3.text == total) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });
                    }
                })

            }
        });
    }
    PantallaDivide() {
        this.centroW = this.game.config.width / 2;
        this.centroH = this.game.config.height / 2;
        let over = this.add.image(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'tabla').setScale(4);
        this.CW = this.sys.game.config.width / 2;
        this.CH = this.sys.game.config.height / 2;
        let finGAme = this.add.text(this.CW - 190, this.CH - 100, 'Perdiste', this.style);

        let nueve = [9, 3, 1];
        let ocho = [8, 4, 2, 1];
        let siete = [7, 1];
        let seis = [6, 3, 1];
        let cinco = [5, 1];
        let cuatro = [4, 2, 1];
        let tres = [3, 1];
        let dos = [2, 1];
        let uno = [1];
        let cero = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];


        // Operacion
        let valor1 = Phaser.Math.Between(0, 9);
        console.log(valor1);

        if (valor1 == 9) {
            let ent = nueve[Phaser.Math.Between(0, 2)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 8) {
            let ent = ocho[Phaser.Math.Between(0, 3)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 7) {
            let ent = siete[Phaser.Math.Between(0, 1)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 6) {
            let ent = seis[Phaser.Math.Between(0, 2)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 5) {
            let ent = cinco[Phaser.Math.Between(0, 1)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 4) {
            let ent = cuatro[Phaser.Math.Between(0, 2)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 3) {
            let ent = tres[Phaser.Math.Between(0, 1)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 2) {
            let ent = dos[Phaser.Math.Between(0, 1)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 1) {
            let ent = uno[0];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }
        if (valor1 == 0) {
            let ent = cero[Phaser.Math.Between(0, 9)];
            this.R = valor1 / ent;
            this.N1 = valor1;
            this.N2 = ent;
        }



        // if (valor2 > valor1) {
        //     valor1 = Phaser.Math.Between(0, 9);
        //     let total = valor1 / valor2;

        // if (total)

        // if (valor2 == 3 || valor2 == 5 || valor2 == 7 || valor2 == 9){

        // }


        let tex = this.add.text(this.CW - 175, this.CH - 45, `Para continuar divide: ${this.N1} / ${this.N2} = ?`, this.style2);

        let r1 = this.physics.add.image(this.CW - 125, this.CH + 23, 'buno');
        let n1 = this.add.text(this.CW - 122, this.CH + 13, '6', this.style2);

        let r2 = this.physics.add.image(this.CW - 20, this.CH + 23, 'bdos');
        let n2 = this.add.text(this.CW - 17, this.CH + 13, '7', this.style2);

        let r3 = this.physics.add.image(this.CW + 85, this.CH + 23, 'btres');
        let n3 = this.add.text(this.CW + 88, this.CH + 13, '8', this.style2);

        this.final = this.add.text(this.CW + 83, this.CH + 60, `${this.R}.`, this.style2);
        this.final.visible = false;

        let tex2 = this.add.text(this.CW - 175, this.CH + 60, `La respuesta corecta es:  `, this.style2);

        n1.setInteractive({ useHandCursor: true });
        n2.setInteractive({ useHandCursor: true });
        n3.setInteractive({ useHandCursor: true });

        n1.text = Phaser.Math.Between(0, 15);
        n2.text = Phaser.Math.Between(0, 15);
        n3.text = Phaser.Math.Between(0, 15);


        this.valores = [];

        this.valores.push(n1.text);
        this.valores.push(n2.text);
        this.valores.push(n3.text);

        console.log(this.valores);


        let int = Phaser.Math.Between(0, 2);

        this.b = this.valores[int]
        console.log(this.b);
        this.b = this.R
        console.log(this.b);

        if (int == 0) {
            n1.text = this.b;
        }
        if (int == 1) {
            n2.text = this.b;
        }
        if (int == 2) {
            n3.text = this.b;
        }


        let contenedorF = this.add.container(0, -400);
        contenedorF.add([over, finGAme, tex, r1, n1, r2, n2, r3, n3, tex2, this.final]);

        this.tweens.add({
            targets: contenedorF,
            ease: 'Pointer1',
            y: 0
        });

        n1.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n1.text == this.R) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });
                    }
                })
            }
        });
        n2.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n2.text == this.R) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });

                    }
                })

            }
        });
        n3.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.final.visible = true;
            if (n3.text == this.R) {
                finGAme.text = 'Continua jugando';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Play', { points: this.points1 });
                    }
                })
            } else {
                tex.text = 'Intentálo luego';
                this.add.tween({
                    targets: contenedorF,
                    ease: 'Pointer1',
                    delay: 2000,
                    y: -300,
                    onComplete: () => {
                        this.scene.start('Menu', { points: this.points1 });
                    }
                })

            }
        });
        // }

    }

    Operaciones() {

        // let OP = [this.PantallaSuma(), this.PantallaResta(), this.PantallaMultiplica(), this.PantallaDivide()];
        let C = Phaser.Math.Between(0, 3);
        if (C == 0) {
            this.PantallaSuma();
        }
        if (C == 1) {
            this.PantallaResta();
        }
        if (C == 2) {
            this.PantallaMultiplica();
        }
        if (C == 3) {
            this.PantallaDivide();
        }
        // return C;
        // }
    }

}
export default OP;