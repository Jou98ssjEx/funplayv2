class Load extends Phaser.Scene {
    constructor() {
        super('Load');
    }

    preload() {
        console.log('Se cargaron todos los recursos');

        this.load.path = '/assets/play1/assets/';

        // Cargando el audio de Load
        this.load.audio('intro', '../sounds/musicLoop.mp3');

        // Simación de la bara de carga
        for (let i = 0; i < 4; i++) {
            this.load.image("caballo" + i, "caballo.png");
        }

        // Crear barra de cargando
        let loadBar = this.add.graphics({
            fillStyle: {
                color: 0xffffff
            }
        });
        // Cargar el evento de la barra
        this.load.on("progress", (percent) => {
            this.add.text(320, 100, "Cargando!....", {
                fontSize: 40
            });

            loadBar.fillRect(0, 200, this.game.renderer.width * percent, 50);
            console.log(percent);
        });
    }

    create() {
        this.s = this.sound.add("intro", { loop: true });
        this.s.play();
        const fontConfig = this.cache.json.get('fontConfig');
        // this.cache.bitmapFont.add('pixelFont', Phaser.GameObjects.RetroFont.Parse(this, fontConfig));
        this.scene.start("Menu", "cargando scena");
    }
}
export default Load;