const mongoose = require('mongoose');
const { Schema } = mongoose;

// const moment = require('moment');
// let now = moment(new Date()).format('LLLL');
// console.log('Fecha Hoy: ' + now);

const EmailSchema = new Schema({
    fullname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String
    },
    affair: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    date: {
        type: String,
        // default: now
    },

});

module.exports = mongoose.model('Email', EmailSchema);