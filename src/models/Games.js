const mongoose = require('mongoose');
const { Schema } = mongoose;

const GameSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    enlace: {
        type: String,
        required: true
    },
    objetivo: {
        type: String,
        required: true
    },
    desafio: {
        type: String,
        required: true
    },
    sipnosis: {
        type: String,
        required: true
    },
    tecla: {
        type: String,
        required: true
    },
    photo: {
        type: String,
        required: true
    },
    user: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    },

});

module.exports = mongoose.model('Game', GameSchema);