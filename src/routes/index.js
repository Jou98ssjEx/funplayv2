const express = require('express');
const router = express.Router();

const { isAuthenticated } = require('../helpers/auth');
const Email = require('../models/Email');
const Games = require('../models/Games');

const moment = require('moment');


router.get('/', (req, res) => {
    res.render('index');
});
router.get('/start', async(req, res) => {
    const games = await Games.find().sort({ date: 'asc' });
    res.render('start', { games });
});
router.get('/emails', isAuthenticated, async(req, res) => {
    const email = await Email.find().sort({ date: 'desc' });
    // const em = await Email.;
    // let d = email.fullname;
    // let a = Object.keys(email);
    // console.log(em);
    // Con esto va a renderizar la pag y pasarle las notas
    res.render('email', { email });
});
router.get('/about', async(req, res) => {
    const games = await Games.find().sort({ date: 'asc' });

    res.render('about', { games });
});
router.get('/contact', (req, res) => {
    res.render('contact');
});
router.get('/game/:id', async(req, res) => {
    const games = await Games.findById(req.params.id);
    res.render('ginfo', { games });
});
// router.get('/grafica', (req, res) => {
//     res.render('grafica');
// });


module.exports = router;