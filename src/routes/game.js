const express = require('express');
const router = express.Router();

const { isAuthenticated } = require('../helpers/auth');
const Games = require('../models/Games');

// Requerir model back
const Note = require('../models/Games');

router.get('/games/add', isAuthenticated, (req, res) => {
    res.render('games/new-game');
});



// Rutas de back

router.post('/games/new-game', async(req, res) => {
    const { title, description, enlace, photo, sipnosis, tecla, objetivo, desafio } = req.body;
    const errors = [];

    if (!title) {
        errors.push({
            text: 'Por favor insertar un Título'
        });
    }
    if (!enlace) {
        errors.push({
            text: 'Por favor insertar el enlace del juego'
        });
    }
    if (!photo) {
        errors.push({
            text: 'Por favor insertar la ruta de la imagen'
        });
    }
    if (!description) {
        errors.push({
            text: 'Por favor insertar una descripción'
        });
    }
    if (!sipnosis) {
        errors.push({
            text: 'Por favor insertar una Sipnosis'
        });
    }
    if (!objetivo) {
        errors.push({
            text: 'Por favor insertar un objetivo'
        });
    }
    if (!desafio) {
        errors.push({
            text: 'Por favor insertar un desafio'
        });
    }
    if (!tecla) {
        errors.push({
            text: 'Por favor insertar la etiqueta corespondiente'
        });
    }
    if (errors.length > 0) {
        res.render('notes/new-note', {
            errors,
            title,
            description,
            enlace,
            photo,
            sipnosis,
            objetivo,
            desafio,
            tecla
        });
    } else {
        const newGame = new Games({
            title,
            description,
            photo,
            enlace,
            sipnosis,
            desafio,
            objetivo,
            tecla
        });
        // Esta line me permite saber el enlace de la nota con el user
        newGame.user = req.user.id;
        await newGame.save();
        req.flash('success_msg', 'Se agrego un juego');
        res.redirect('/admin');
    }
});


// Esta ruta te permite traer la vista para editar, debe llamar a un formulario

router.get('/games/edit/:id', isAuthenticated, async(req, res) => {
    const game = await Games.findById(req.params.id);
    res.render('games/edit-game', { game });
});

// Esta ruta hace la peticion put para actualizar la db

router.put('/games/edit-game/:id', isAuthenticated, async(req, res) => {
    const { title, description, enlace, photo, sipnosis, objetivo, desafio, tecla } = req.body;
    await Note.findByIdAndUpdate(req.params.id, { title, description, enlace, photo, sipnosis, objetivo, desafio, tecla });
    req.flash('success_msg', 'La información se actualizó correctamente');
    res.redirect('/admin');
});

// para eliminar la nota

router.delete('/games/delete/:id', isAuthenticated, async(req, res) => {
    await Games.findByIdAndDelete(req.params.id);
    req.flash('success_msg', 'El juego se eliminó correctamente');
    res.redirect('/admin');
});


module.exports = router;