const express = require('express');
const router = express.Router();

// Requerir model back
const Email = require('../models/Email');
const { isAuthenticated } = require('../helpers/auth');

const moment = require('moment');



router.post('/send-email', async(req, res) => {
    const { fullname, email, phone, affair, message } = req.body;

    const errors = [];

    if (!fullname) {
        errors.push({
            text: 'Por favor insertar su Nombre'
        });
    }
    if (!email) {
        errors.push({
            text: 'Por favor insertar su email'
        });
    }
    if (!phone) {
        errors.push({
            text: 'Por favor insertar su número de telefono'
        });
    }
    if (!affair) {
        errors.push({
            text: 'Por favor insertar su asunto'
        });
    }
    if (!message) {
        errors.push({
            text: 'Por favor insertar una descripción'
        });
    }
    if (errors.length > 0) {
        res.render('contact', {
            errors,
            fullname,
            email,
            phone,
            affair,
            message

        });
    } else {

        moment.locale('es');
        let date = moment(new Date()).format('LLLL');
        console.log('Fecha Hoy: ' + date);

        const newEmail = new Email({
            fullname,
            email,
            phone,
            affair,
            message,
            date
        });
        await newEmail.save();
        req.flash('success_msg', 'Se Envió el formulario de Contacto');
        res.redirect('/contact');
    }
    // res.send('ok')
});

// para eliminar la nota

router.delete('/email/delete/:id', isAuthenticated, async(req, res) => {
    await Email.findByIdAndDelete(req.params.id);
    req.flash('success_msg', 'El correo se eliminó correctamente');
    res.redirect('/emails');
});


// router.get('/emails', isAuthenticated, async(req, res) => {
//     const email = await Email.find({ user: req.user.id }).sort({ date: 'desc' });
//     // Con esto va a renderizar la pag y pasarle las notas
//     res.render('/email', { email });
// });

module.exports = router;