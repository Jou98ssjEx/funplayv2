const express = require('express');
const router = express.Router();
const User = require('../models/User');
// const Games = require('../models/Games');
const passport = require('passport');
const session = require('express-session');

const moment = require('moment');


const { isAuthenticated } = require('../helpers/auth');
const Games = require('../models/Games');


router.get('/users/signin', (req, res) => {
    res.render('./users/signin');
});

// Autenticar Usuario
router.post('/users/signin', passport.authenticate('local', {
    successRedirect: '/admin',
    failureRedirect: '/users/signin',
    failureFlash: 'Por favor ingresar sus credenciales!'
}));

// Autenticar con Gooogle
router.get('/google', passport.authenticate('google', {
    scope: ['profile']

}));

//  callback de google
router.get('/auth/google/redirect', passport.authenticate('google', {
    successRedirect: '/admin',
    failureRedirect: '/users/signin',
    failureFlash: 'Por favor ingresar sus credenciales!'
}));


// // Autenticar con Facebook
// router.get('/auth/facebook', passport.authenticate('facebook', {
//     // scope: ['profile']
//     authType: 'rerequest'
//         // scope: ['user_friends']

// }));

// //  callback de facebook
// // router.get('/auth/facebook/callback/', passport.authenticate('facebook'), (req, res) => {
// //     // res.send(req.user);
// //     res.render('/notes');
// // });

// router.get('/auth/facebook/callback/', passport.authenticate('facebook', {
//     successRedirect: '/notes',
//     failureRedirect: '/',
//     failureFlash: 'Por favor ingresar sus credenciales!'
// }));


router.get('/users/signup', (req, res) => {
    res.render('./users/signup');
});

router.post('/users/signup', async(req, res) => {
    const { name, email, password, confirm_password } = req.body;
    const errors = [];

    if (name == '' || email == '' || password == '' || confirm_password == '') {
        errors.push({
            text: 'Ingrese todos los campos'
        });
    }
    if (password != confirm_password) {
        errors.push({
            text: 'Las contraseñas no son iguales'
        });
    }
    if (password.length < 4) {
        errors.push({
            text: 'Las contraseñas debe tener mas de 4 caracteres'
        });
    }
    const emailUser = await User.findOne({ email: email });
    if (emailUser) {
        errors.push({
            text: 'El correo ingresado ya esta en uso'
        });
    }
    if (errors.length > 0) {
        res.render('users/signup', {
            errors,
            // name,
            // password,
            // email,
            // confirm_password
        });

    } else {
        const newUser = new User({
            name,
            email,
            password
        });
        newUser.password = await newUser.encryptPassword(password);
        await newUser.save();
        req.flash('success_msg', 'El usuario se registro corectamente');
        res.redirect('/users/signin');
    }

});

// Cerrar sesion

router.get('/users/logout', isAuthenticated, (req, res) => {
    req.logout();
    res.redirect('/');
});


router.get('/admin', isAuthenticated, async(req, res) => {
    // const games = await Games.find({ user: req.user.id }).sort({ date: 'desc' });

    // let now = moment(new Date()).format('YYYY-MM-DD[T00:00:00.000Z]');
    // let now = moment(new Date()).format('DD-MM-YYYY h:mma');
    moment.locale('es');
    let now = moment(new Date()).format('LLLL');
    console.log('Fecha Hoy: ' + now);
    const games = await Games.find().sort({ date: 'asc' });
    res.render('./users/admin', { games, now });
});

module.exports = router;